package edu.usf.cutr.transporttracker.model;

import com.google.firebase.database.ServerValue;

public class Post {
    public int likeCount;
    private Long createDate;
    public String createBy;

    public java.util.Map<String, String> getCreationDate() {
        return ServerValue.TIMESTAMP;
    }


    public Long getCreateDateLong() {
        return createDate;
    }

    public Post() {
    }
}
