package edu.usf.cutr.transporttracker.model;

public class LatLngModel {
   private double lat;
   private double lng;
   private double speed;
   private double acc;
   private double Rad;
   private long time;
   private String rideType;
   private boolean isRunning;
   private double bearing;


    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getRad() {
        return Rad;
    }

    public void setRad(double Rad) {
        this.Rad = Rad;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getRideType() {
        return rideType;
    }

    public void setRideType(String rideType) {
        this.rideType = rideType;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void setRunning(boolean running) {
        isRunning = running;
    }

    public double getBr() { return bearing;    }

    public void setBr(double bearing) {
        this.bearing = bearing;
    }

}
