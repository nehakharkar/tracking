package edu.usf.cutr.transporttracker.model;

public class DistanceList {

    private double dist;
    public DistanceList(double dist){
        this.dist = dist;
    }

    public double getDist() {
        return dist;
    }

    public void setDist(double dist) {
        this.dist = dist;
    }

}
