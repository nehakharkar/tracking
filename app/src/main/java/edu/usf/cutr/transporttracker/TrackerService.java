package edu.usf.cutr.transporttracker;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.maps.android.SphericalUtil;

import java.util.ArrayList;
import java.util.List;
import java.lang.*;
import edu.usf.cutr.transporttracker.model.LatLngModel;
import edu.usf.cutr.transporttracker.model.MyLocation;
import edu.usf.cutr.transporttracker.model.Post;
import edu.usf.cutr.transporttracker.model.PredictedLocations;
import edu.usf.cutr.transporttracker.model.DistanceList ;


public class TrackerService extends Service implements SensorEventListener {
    private double d = 0;
    private static final String NEHA = "Nishant";
    private static final String TAG = TrackerService.class.getSimpleName();
    private List<MyLocation> myLocations = new ArrayList<>();
    private List<DistanceList> CalDistance = new ArrayList<>();
    private List<PredictedLocations> PredictedLocation = new ArrayList<>();
    private ArrayList PredictBear = new ArrayList();
    private float lastX, lastY, lastZ;
    private SensorManager sensorManager;
    private Sensor accelerometer;
    private float currentX = 0;
    private float currentY = 0;
    private float currentZ = 0;
    private float deltaX = 0;
    private float deltaY = 0;
    private float deltaZ = 0;
    private float vibrateThreshold = 0;
    private float timestamp;
    public Vibrator v;
    private CountDownTimer countDownTimer;
    private LocationRequest request;
    private FusedLocationProviderClient client;
    private long[] elapsedTime;
    private final int alertNotification = 1310, alertCrashNotification = 2610;
    private boolean isNotificationShow;
    private OnValueUpdater onValueUpdater;
    private final IBinder binder = new LocalBinder();
    private String currentId = "";
    private LatLngModel latLngModel;
    private DistanceList distanceCal;
    private double speed;
    private String rideType;
    private double Radius;
    private float acceleration;
    private double previousDistance=0;
    private double distance;
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Bundle extras = intent.getExtras();
        if (extras == null) {
            rideType = null;
        } else {
            rideType = extras.getString("rideType");
        }

        Log.d(NEHA, "rideType"+ rideType );

        return super.onStartCommand(intent, flags, startId);
    }

    public class LocalBinder extends Binder {
        TrackerService getService() {
            // Return this instance of MyService so clients can call public methods
            return TrackerService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        buildNotification();
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
            // success! we have an accelerometer
            accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
            vibrateThreshold = accelerometer.getMaximumRange() / 2;
        } else {

            // fail! we dont have an accelerometer!

        }

        //initialize vibration
        v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        requestLocationUpdates();
        updateLastLng(false);

        createFirebaseListerner();
    }

    private void createFirebaseListerner() {
        final String firepath2 = getString(R.string.firebase_parentpathLat);
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(firepath2);
        databaseReference.addChildEventListener(childEventListener);
    }

    private void removeFirebaseListerner() {
        final String firepath2 = getString(R.string.firebase_parentpathLat);
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(firepath2);
        databaseReference.removeEventListener(childEventListener);
    }

    ChildEventListener childEventListener = new ChildEventListener() {

        @Override
        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

        }

        @Override
        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {


            if (!dataSnapshot.getKey().equals(currentId)) {
                try {
                    if(distance > 0) {
                        previousDistance = distance;
                    }
                     distance = SphericalUtil.computeDistanceBetween(new LatLng(latLngModel.getLat(), latLngModel.getLng()),
                            new LatLng(dataSnapshot.getValue(LatLngModel.class).getLat(), dataSnapshot.getValue(LatLngModel.class).getLng()));

                    Double Bearing1= latLngModel.getBr();
                    Double Bearing2 = dataSnapshot.getValue(LatLngModel.class).getBr();
                    Double DifferenceBearing = Math.abs(Bearing1 - Bearing2);
                    Log.d(NEHA, "Bear1"+ Bearing1 + "Bear2" + Bearing2 +" "+DifferenceBearing);

                   /*if (Double.isNaN(BearingDifference)|| Double.isInfinite(BearingDifference) ) {
                        BearingDifference = 0d;
                    }
                    */
                    String timeDataDistance = dataSnapshot.getValue(LatLngModel.class).getTime() + "";
                    String keyDistance = dataSnapshot.getKey();
                    final String firepath = getString(R.string.firebase_parentpath);
                    DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(firepath);
                    databaseReference.child(keyDistance).child(timeDataDistance).child("distance").setValue(distance);
                    Log.d(TAG, "previousDistance"+ previousDistance + "Distance" + distance );


                    String RideType1 = dataSnapshot.getValue(LatLngModel.class).getRideType();
                    String RideType2 = latLngModel.getRideType();
                    Log.d(NEHA, "RideType1"+ RideType1 + "RideType2"+ RideType2);
                    Double newvalue = Double.parseDouble(getString(R.string.increase_radius));
                    FirebaseAuth fbAuth;
                    Post np = new Post();
                    fbAuth = FirebaseAuth.getInstance();
                    np.createBy = fbAuth.getCurrentUser().getUid();

                    if(rideType.equals(getString(R.string.car)) )
                    {
                        if(DifferenceBearing >160 && DifferenceBearing <200 )
                        {

                            String t = latLngModel.getTime() + "";
                            databaseReference.child(np.createBy).child(t).child("radius").setValue(newvalue);
                            //MyRadius1 = newvalue;
                            latLngModel.setRad(newvalue);

                            if ((distance < 40) && (distance>newvalue) &&(previousDistance > distance) && !(RideType1.equals(RideType2)) && (RideType1.equals(getString(R.string.bicycle))))
                            {
                                String timeData = latLngModel.getTime() + "";
                                /*if (dataSnapshot.getValue(LatLngModel.class) != null && rideType.equals())
                                    alertCrashUser(getString(R.string.notification_text_crash_alert_car));
                                else
                                alertCrashUser(getString(R.string.notification_text_crash_alert_bicycle));*/

                                alertCrashUser(getString(R.string.notification_text_crash_alert_Scenario3_Car));
                                databaseReference.child(np.createBy).child(timeData).child("userAlert").setValue(true);
                            }
                        }

                    }
                    String BearTime = dataSnapshot.getValue(LatLngModel.class).getTime() + "";
                    String BKey = dataSnapshot.getKey();
                    databaseReference.child(BKey).child(BearTime).child("DifferenceBearing").setValue(DifferenceBearing);

                    Double MyRadius1 = latLngModel.getRad();
                    Double MyRadius2 = dataSnapshot.getValue(LatLngModel.class).getRad();
                   //Log.d(NEHA, "MyRad1"+ MyRadius1 + "MyRadius2" + MyRadius2 +" "+np.createBy);

                    Double finalRadius = MyRadius1 + MyRadius2;

                    if ((distance < finalRadius) && (previousDistance > distance) && !(RideType1.equals(RideType2)))
                    {
                        String timeData = dataSnapshot.getValue(LatLngModel.class).getTime() + "";
                        String key = dataSnapshot.getKey();

                        if(DifferenceBearing >0 && DifferenceBearing <20 )
                        {
                            if (rideType.equals(getString(R.string.bicycle)) &&  RideType1.equals(getString(R.string.car)))
                            {
                                alertCrashUser(getString(R.string.notification_text_crash_alert_Scenario2_Bike));
                            }
                            else if (rideType.equals(getString(R.string.car))  && RideType1.equals(getString(R.string.bicycle)))
                            {
                                alertCrashUser(getString(R.string.notification_text_crash_alert_Scenario2_Car));
                            }
                        }
                        else if( DifferenceBearing >75 && DifferenceBearing <105)
                        {
                            if (rideType.equals(getString(R.string.bicycle)) && RideType1.equals(getString(R.string.car)))
                            {
                                alertCrashUser(getString(R.string.notification_text_crash_alert_Scenario1_Bike));
                            }
                            else if (rideType.equals(getString(R.string.car)) && RideType1.equals(getString(R.string.bicycle)))
                            {
                                alertCrashUser(getString(R.string.notification_text_crash_alert_Scenario1_Car));
                            }
                        }
                        else if (DifferenceBearing >160 && DifferenceBearing <200 )
                        {
                            if (rideType.equals(getString(R.string.bicycle)) && RideType1.equals(getString(R.string.car)))
                            {
                                alertCrashUser(getString(R.string.notification_text_crash_alert_Scenario3_Bike));
                            }
                        }
                        else
                            {
                                alertCrashUser(getString(R.string.notification_text_crash_alert));
                        }
                        databaseReference.child(key).child(timeData).child("userAlert").setValue(true);
                    }


                    Log.i("Distance", distance + "");
                } catch (Exception ae) {
                    ae.printStackTrace();
                }
            }
        }

        @Override
        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

        }

        @Override
        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    private void buildNotification() {
        String stop = "stop";
        registerReceiver(stopReceiver, new IntentFilter(stop));

        String id = "TRACK_NOTIFICATION";
        NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= 26) {
            CharSequence name = "TRACK";
            String description = "TRACK";
            NotificationChannel mChannel = new NotificationChannel(id, name,
                    NotificationManager.IMPORTANCE_LOW);
            mChannel.setDescription(description);
            mChannel.setShowBadge(false);
            mChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            mNotifyMgr.createNotificationChannel(mChannel);
        }
        PendingIntent broadcastIntent = PendingIntent.getBroadcast(
                this, 0, new Intent(stop), PendingIntent.FLAG_UPDATE_CURRENT);
        // Create the persistent notification
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.app_name))
                .setWhen(System.currentTimeMillis())
                .setContentText(getString(R.string.notification_text))
                .setOngoing(true)
                .setChannelId(id)
                .setContentIntent(broadcastIntent)
                .setSmallIcon(R.drawable.ic_stat_name);
        startForeground(1, builder.build());
    }


    private void alertCrashUser(String message) {
        String id = "TRACK_NOTIFICATION_CRASH_ALERT";
        NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= 26) {
            CharSequence name = "TRACK";
            String description = "TRACK";
            NotificationChannel mChannel = new NotificationChannel(id, name,
                    NotificationManager.IMPORTANCE_HIGH);
            mChannel.setDescription(description);
            mChannel.setShowBadge(false);
            mChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            mNotifyMgr.createNotificationChannel(mChannel);
        }

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(), PendingIntent.FLAG_UPDATE_CURRENT);

        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        // Create the persistent notification
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(message)
                .setChannelId(id)
                .setWhen(System.currentTimeMillis())
                .setContentIntent(contentIntent)
                .setSound(soundUri)
                .setSmallIcon(R.drawable.ic_stat_name);

        mNotifyMgr.notify(alertCrashNotification, builder.build());
    }

    protected BroadcastReceiver stopReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "received stop broadcast");
            // Stop the service when the notification is tapped
            clearAll();
        }
    };


    public void requestLocationUpdates() {

        request = new LocationRequest();
        request.setInterval(0);
        //request.setFastestInterval(0);
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        client = LocationServices.getFusedLocationProviderClient(this);


        final String path = getString(R.string.firebase_path) + "/" + getString(R.string.transport_id);
        int permission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        final double r = 6371; // Earth Radius in m


        if (permission == PackageManager.PERMISSION_GRANTED) {

            // Request location updates and when an update is
            // received, store the location in Firebase
            client.requestLocationUpdates(request, locationCallback, null);

        }


    }

    LocationCallback locationCallback = new LocationCallback() {

        @Override
        public void onLocationResult(LocationResult locationResult) {
            try {

                Location location = locationResult.getLastLocation();

                Log.d(TAG, " location updates " + location.getLongitude() + ' ' + location.getLatitude() + ' ' + location.getAltitude() + ' ' + location.getTime() + ' ' + location.getAccuracy() + ' ' + location.getSpeed() + ' ' + location.getBearing());

                // if you want to convert in km then getSpeed*3.6
                double kmDistance = location.getSpeed() * (0.1 / 1000f) / 3600f;
                //double distance = kmDistance / 1000;

                double speed = location.getSpeed();
                double acceleration = displayCurrentValueY();

                //double Radius = (0.695 * speed) + ((speed * speed) / (254 * (acceleration / 9.81)));
			    double Radius = (2.5 * speed) + ((speed * speed) / (2 * 9.80991*(acceleration / 9.80991)));
                double CalcRad=Radius;
                if (Double.isNaN(Radius)|| Double.isInfinite(Radius)) {
                    Radius = 0;
                }
                if (rideType.equals(getString(R.string.bicycle))) {
                    double value = Double.parseDouble(getString(R.string.bicycle_value));
                    if (Radius <= value) {
                        Radius = value;
                    }
                } else if (rideType.equals(getString(R.string.car))) {
                    double value = Double.parseDouble(getString(R.string.car_value));
                    if (Radius <= value) {
                        Radius = value;
                    }
                } else if (rideType.equals(getString(R.string.other))) {
                    double value = Double.parseDouble(getString(R.string.other_value));
                    if (Radius <= value) {
                        Radius = value;
                    }
                }

                //data added to list
                myLocations.add(new MyLocation(
                        location.getLatitude(),
                        location.getLongitude(),
                        location.getTime(),
                        location.getAccuracy(),
                        location.getSpeed(),
                        location.getBearing(),
                        location.getAltitude(),
                        displayCurrentValueX(),
                        displayCurrentValueY(),
                        displayCurrentValueZ(), Radius,CalcRad));


                writeToDatabase();

            }
            catch (Exception ae){
                ae.printStackTrace();
            }
        }


    };


    private void writeToDatabase() {
        try {
            FirebaseAuth firebaseAuth;
            Post newPost = new Post();
            firebaseAuth = FirebaseAuth.getInstance();
            newPost.createBy = firebaseAuth.getCurrentUser().getUid();
            currentId = firebaseAuth.getCurrentUser().getUid();
            final double r = 6371;
            final String firepath2 = getString(R.string.firebase_parentpath);
            DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(firepath2);
            Log.d(TAG, " check location" + myLocations.size() + myLocations );

            for (int i = 0; i < myLocations.size(); ++i) {
                //WRITING TO DATABASE


                try {
                    if (Double.isNaN(myLocations.get(i).getRadius()) || Double.isInfinite(myLocations.get(i).getRadius())) {
                        myLocations.get(i).setRadius(0);
                    }
                    if (Double.isNaN(myLocations.get(i).getCalcRad())  || Double.isInfinite( myLocations.get(i).getCalcRad()) ) {
                        myLocations.get(i).setCalcRad(0);
                    }

                    databaseReference.child(newPost.createBy).child(String.valueOf(myLocations.get(i).getTime())).setValue(myLocations.get(i));

                    Log.d(TAG, " newacce:" + displayCurrentValueX() + " " + displayCurrentValueY() + " " + displayCurrentValueZ());


                } catch (DatabaseException exception) {
                    exception.printStackTrace();
                }



            }

            updateLastLng(true);
            myLocations.clear();

        }
        catch (Exception ae){
            ae.printStackTrace();
        }


    }

    private void updateLastLng(boolean isStatus) {
        try {
            Log.d(TAG, " check locat " + myLocations.size() + myLocations );

            if (myLocations != null && myLocations.size() > 0) {
                FirebaseAuth firebaseAuth;
                Post newPost = new Post();
                firebaseAuth = FirebaseAuth.getInstance();
                newPost.createBy = firebaseAuth.getCurrentUser().getUid();
                latLngModel = new LatLngModel();
                latLngModel.setLat(myLocations.get(myLocations.size() - 1).getLatitude());
                latLngModel.setLng(myLocations.get(myLocations.size() - 1).getLongitude());
                latLngModel.setSpeed(myLocations.get(myLocations.size() - 1).getSpeed());
                latLngModel.setBr(myLocations.get(myLocations.size() - 1).getBear());
                //latLngModel.setAcceleration(myLocations.get(myLocations.size() - 1).getAccelerationY());
                speed = myLocations.get(myLocations.size() - 1).getSpeed();
                acceleration = myLocations.get(myLocations.size() - 1).getAccelerationY();
                Radius = (0.695 * speed) + ((speed * speed) / (254 * (acceleration / 9.81)));
                if (Double.isNaN(Radius) || Double.isInfinite(Radius)) {
                    Radius = 0;
                }
                if (rideType.equals(getString(R.string.bicycle))) {
                    double value = Double.parseDouble(getString(R.string.bicycle_value));
                    if (Radius <= value) {
                        Radius = value;
                    }
                } else if (rideType.equals(getString(R.string.car))) {
                    double value = Double.parseDouble(getString(R.string.car_value));
                    if (Radius <= value) {
                        Radius = value;
                    }
                } else if (rideType.equals(getString(R.string.other))) {
                    double value = Double.parseDouble(getString(R.string.other_value));
                    if (Radius <= value) {
                        Radius = value;
                    }
                }
                latLngModel.setRad(Radius);
                latLngModel.setRideType(rideType);
                latLngModel.setTime(myLocations.get(myLocations.size() - 1).getTime());
                latLngModel.setRunning(isStatus);
                final String firepath2 = getString(R.string.firebase_parentpathLat);
                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(firepath2);
                databaseReference.child(newPost.createBy).setValue(latLngModel);
            }
            else {
                Log.d(TAG, " check neha " + myLocations.size() + myLocations );

                FirebaseAuth firebaseAuth;
                Post newPost = new Post();
                firebaseAuth = FirebaseAuth.getInstance();
                newPost.createBy = firebaseAuth.getCurrentUser().getUid();
                final String firepath2 = getString(R.string.firebase_parentpathLat);
                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(firepath2);
                databaseReference.child(newPost.createBy).child("running").setValue(isStatus);
                databaseReference.child(newPost.createBy).child("rideType").setValue(rideType);
            }
        }
        catch (Exception ae){
            ae.printStackTrace();
        }

    }


    static public double initial(double lat1, double long1, double lat2, double long2) {
        return (_bearing(lat1, long1, lat2, long2) + 360.0) % 360;
    }


    static private double _bearing(double lat1, double long1, double lat2, double long2) {
        double degToRad = Math.PI / 180.0;

        double phi1 = lat1 * degToRad;
        double phi2 = lat2 * degToRad;

        double lam1 = long1 * degToRad;
        double lam2 = long2 * degToRad;

        return Math.atan2(Math.sin(lam2 - lam1) * Math.cos(phi2), Math.cos(phi1) * Math.sin(phi2) - Math.sin(phi1) * Math.cos(phi2) * Math.cos(lam2 - lam1)) * 180 / Math.PI;
    }



    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // can be safely ignored for this demo
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        // clean current values
        // display the current x,y,z accelerometer values
        displayCurrentValueX();
        displayCurrentValueY();
        displayCurrentValueZ();
        // display the max x,y,z accelerometer values

        // get the change of the x,y,z values of the accelerometer
        deltaX = Math.abs(lastX - event.values[0]);
        deltaY = Math.abs(lastY - event.values[1]);
        deltaZ = Math.abs(lastZ - event.values[2]);

        // if the change is below 2, it is just plain noise
        if (deltaX < 2)
            deltaX = 0;
        if (deltaY < 2)
            deltaY = 0;
        if (deltaZ < 2)
            deltaZ = 0;


    }


    // display the current x,y,z accelerometer values
    public float displayCurrentValueX() {
        currentX = deltaX;
        //Log.e("acce", "X:" + deltaX);
        return currentX;
    }

    public float displayCurrentValueY() {
        currentY = deltaY;
        //Log.e("acce", "Y:" + deltaY);
        return currentY;
    }

    public float displayCurrentValueZ() {
        currentZ = deltaZ;
        //Log.e("acce", "Z:" + deltaZ);
        return currentZ;
    }


    public static double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515 * 1.609344;

        return (dist);
    }

    /**
     * <p>This function converts decimal degrees to radians.</p>
     *
     * @param deg - the decimal to convert to radians
     * @return the decimal converted to radians
     */
    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /**
     * <p>This function converts radians to decimal degrees.</p>
     *
     * @param rad - the radian to convert
     * @return the radian converted to decimal degrees
     */
    private static double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }


    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        clearAll();

    }

    private void clearAll() {
        updateLastLng(false);
        removeFirebaseListerner();
        if (stopReceiver != null)
            unregisterReceiver(stopReceiver);
        stopSelf();
        if (countDownTimer != null)
            countDownTimer.cancel();
        if (client != null && locationCallback != null)
            client.removeLocationUpdates(locationCallback);
        NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotifyMgr.cancel(alertNotification);
        mNotifyMgr.cancel(1);

    }

    public void setCallbacks(OnValueUpdater onValueUpdater) {
        this.onValueUpdater = onValueUpdater;
    }

    // callback or interface which pass data from service to activity
    public interface OnValueUpdater {
        void valueUpdate(double value);
    }
}