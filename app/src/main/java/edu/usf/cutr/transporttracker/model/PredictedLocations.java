package edu.usf.cutr.transporttracker.model;

public class PredictedLocations {
    private double pLatitude;
    private double pLongitude;
    private long pTime;

    public PredictedLocations(double lat, double lon, long time){
        this.pLatitude=lat;
        this.pLongitude= lon;
        this.pTime=time;


    }

    public double getpredictLat() {
        return pLatitude;
    }



    public double getpredictLong() {
        return pLongitude;
    }


    public long getpredictTime() {
        return pTime;
    }


}
