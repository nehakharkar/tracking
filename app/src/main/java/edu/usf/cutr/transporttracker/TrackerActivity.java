package edu.usf.cutr.transporttracker;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import static android.content.ContentValues.TAG;

public class TrackerActivity extends AppCompatActivity implements TrackerService.OnValueUpdater, LocationListener {

    private static final int PERMISSIONS_REQUEST = 1;
    private Toolbar toolbar;
    private static final int RC_SIGN_IN = 123;
    private boolean isRunning;
    private TrackerService myService;
    private TextView tvSpeed;
    private Button tvStopActivity;
    private String rideType;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracker);
        Bundle extras = getIntent().getExtras();
        if(extras == null) {
            rideType= null;
        } else {
            rideType= extras.getString("rideType");
        }

        Log.d(TAG, "login in success");

        Toolbar toolbar = findViewById(R.id.toolbar);
        tvSpeed = findViewById(R.id.tv_speed);
        tvStopActivity = findViewById(R.id.stopActivity);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Trackingactivity");


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Toast.makeText(this, "Please enable location services", Toast.LENGTH_SHORT).show();
            finish();
        }

        // Check location permission is granted - if it is, start
        // the service, otherwise request the permission
        final int permission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSIONS_REQUEST);
        }



        tvStopActivity.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String stop = "stop";
                Intent iv = new Intent(stop);
                sendBroadcast(iv);
                finish();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            startTrackerService();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (FirebaseAuth.getInstance().getCurrentUser() != null && isRunning) {
            myService.setCallbacks(null);
            unbindService(serviceConnection);
        }
    }

    private void startTrackerService() {

        // For getting data from service. starting servie as bind service. So that it helps me to getting data from servie
        Intent intent = new Intent(this, TrackerService.class);
        intent.putExtra("rideType", rideType);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
        startService(intent);
    }


    // service connection object. it is connected to service
    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            isRunning = true;
            TrackerService.LocalBinder binder = (TrackerService.LocalBinder) service;
            myService = binder.getService();
            myService.setCallbacks(TrackerActivity.this);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            isRunning = false;
        }
    };


    // interface method for callback
    @Override
    public void valueUpdate(double value) {
        Log.i("TAG", "data -> " + value);
        tvSpeed.setText("Speed: " + value);
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i("TAG", "Inner " + location.getSpeed());
//        tvSpeed.setText("Inner: " + location.getSpeed());
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


/*

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[]
            grantResults) {
        if (requestCode == PERMISSIONS_REQUEST && grantResults.length == 1
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // Start the service when the permission is granted
            startTrackerService();
        } else {
            finish();
        }
    }

*/


}