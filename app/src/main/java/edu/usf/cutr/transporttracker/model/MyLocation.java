package edu.usf.cutr.transporttracker.model;

public class MyLocation {

    private double mLatitude;
    private double mLongitude;
    private long mTime;
    private float mAccuracy;
    private float mSpeed;
    private float mBear;
    private float oBear;
    private float DifferenceBearing;
    private double mAltitude;
    private float mAccelerationX;
    private float mAccelerationY;
    private float mAccelerationZ;
    private double radius;
    private double calcRad;
    private double distance;
    private boolean isUserAlert;

    public MyLocation(double mLatitude, double mLongitude, long mTime, float mAccuracy, float mSpeed, float mBear, float oBear,double mAltitude, float mAccelerationX, float mAccelerationY, float mAccelerationZ, double radius,double calcRad) {
        this.mLatitude = mLatitude;
        this.mLongitude = mLongitude;
        this.mTime = mTime;
        this.mAccuracy = mAccuracy;
        this.mSpeed = mSpeed;
        this.mBear = mBear;
        this.oBear = oBear;
        this.mAltitude = mAltitude;
        this.DifferenceBearing= 0;
        this.mAccelerationX = mAccelerationX;
        this.mAccelerationY = mAccelerationY;
        this.mAccelerationZ = mAccelerationZ;
        this.radius = radius;
        this.calcRad=calcRad;
        this.isUserAlert = false;
        this.distance = 0;

    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double mLatitude) {
        this.mLatitude = mLatitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setmLongitude(double mLongitude) {
        this.mLongitude = mLongitude;
    }



    public long getTime() {
        return mTime;
    }

    public void setTime(long mTie) {
        this.mTime = mTime;
    }

    public float getAccuracy() {
        return mAccuracy;
    }

    public void setAccuracy(float mAccuracy) {
        this.mAccuracy = mAccuracy;
    }

    public float getSpeed() {
        return mSpeed;
    }

    public void setSpeed(float mSpeed) {
        this.mSpeed = mSpeed;
    }

    public float getBear() { return mBear;    }

    public void setBear(float mBear) {  this.mBear = mBear;   }

    public float getorigBear() { return oBear;    }

    public void setorigBear(float oBear) {  this.oBear = oBear;   }

    public double getAltitude() {
        return mAltitude;
    }

    public void setAltitude(double mAltitude) {
        this.mAltitude = mAltitude;
    }

    public float getAccelerationX() {
        return mAccelerationX;
    }

    public void setAccelerationX(float mAccelerationX) {
        this.mAccelerationX = mAccelerationX;
    }

    public float getAccelerationY() {
        return mAccelerationY;
    }

    public void setAccelerationY(float mAccelerationY) {
        this.mAccelerationY = mAccelerationY;
    }

    public float getAccelerationZ() {
        return mAccelerationZ;
    }

    public void setAccelerationZ(float mAccelerationZ) {
        this.mAccelerationZ = mAccelerationZ;
    }

    public double getCalcRad() {
        return calcRad;
    }

    public void setCalcRad(double calcRad) {
        this.calcRad = calcRad;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public boolean isUserAlert() {
        return isUserAlert;
    }

    public void setUserAlert(boolean userAlert) {
        isUserAlert = userAlert;
    }
}
